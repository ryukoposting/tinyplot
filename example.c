#include "plot.h"
#include <pthread.h>
#include <math.h>
#include <time.h>
#include <assert.h>

#define MAX_NTASKS 8

enum status {
    STATUS_OK,
    STATUS_ERR
};

struct task {
    unsigned int period, deadline, cost, release;
};

static struct task g_tsktable[MAX_NTASKS];
static unsigned int g_ntasks = 0;
static pthread_mutex_t g_tasks_mutex;


enum status maketask(unsigned int deadline, unsigned int period, unsigned int cost, unsigned int release)
{
    enum status out = STATUS_OK;
    
    pthread_mutex_lock(&g_tasks_mutex);
    
    if (g_ntasks < MAX_NTASKS) {
        struct task *newtask = g_tsktable + g_ntasks;
        newtask->period = period;
        newtask->deadline = deadline;
        newtask->cost = cost;
        newtask->release = release;
        g_ntasks += 1;
    
    } else out = STATUS_ERR; // max number of tasks already created
    
    pthread_mutex_unlock(&g_tasks_mutex);
    
    return out;
}

// U
double utilization()
{
    pthread_mutex_lock(&g_tasks_mutex);
    
    double out = 0.0;
    for (struct task *t = g_tsktable; t < g_tsktable + g_ntasks; ++t) {
        out += t->cost / t->deadline;
    }
    
    pthread_mutex_unlock(&g_tasks_mutex);
    
    return out;
}

// H(t)
double demand(double t)
{
    pthread_mutex_lock(&g_tasks_mutex);
    
    double out = 0.0;
    for (struct task *tsk = g_tsktable; tsk < g_tsktable + g_ntasks; ++tsk) {
        out += floor((t - tsk->deadline + tsk->period) / tsk->period) * tsk->cost;
    }
    
    pthread_mutex_unlock(&g_tasks_mutex);
    
    return out;
}

// W(t)
double workload(double t)
{
    pthread_mutex_lock(&g_tasks_mutex);
    
    double out = 0.0;
    for (struct task *tsk = g_tsktable; tsk < g_tsktable + g_ntasks; ++tsk) {
        out += ceil(t / tsk->period) * tsk->cost;
    }
    
    pthread_mutex_unlock(&g_tasks_mutex);
    
    return out;
}


double fn(double x)
{
    return x - (0.0001 * x * x * x) - (0.01 * x * x);
}


void draw_function_rec(struct plot_img *img, double const xmax, double (*const fn)(double),
                                             double const x0, double const y0,
                                             double const offsetx, double const offsety,
                                             double scalex, double scaley)
{
    double const x1 = x0 + (1.0 / scalex);
    if (x1 >= xmax) return;
    
    double const y1 = fn(x1);
    
    printf("%f %f %f %f\n", x0 + offsetx, offsety - y0, x1 + offsetx, offsety - y1);
    
    img_drawline(img, (x0 * scalex) + offsetx,
                      offsety - (y0 * scaley),
                      (x1 * scalex) + offsetx, 
                      offsety - (y1 * scaley));
    
    draw_function_rec(img, xmax, fn, x1, y1, offsetx, offsety, scalex, scaley);
}

//     draw_function(&img, 0, 16, demand, 10, 390, 16.0, 16.0);
void draw_function(struct plot_img *img, double xmin, double xmax, double (*const fn)(double),
                                         int offsetx, int offsety,
                                         double scalex, double scaley)
{
    draw_function_rec(img, xmax, fn,
                      xmin, fn(xmin),
                      offsetx, offsety, scalex, scaley);
}


void sierpinski_carpet(struct plot_img *img, double const xo, double const yo,
                                             double const x1, double const y1,
                                             int depth, int wallsize)
{
    double const xa = x1 / 3;
    double const ya = y1 / 3;
    
    if ((depth <= 0) || (trunc(xa) == 0) || (trunc(ya) == 0)) return;
    
    static double color = 0;
    img_setpenhsv(img, color, 1.0, 1.0);
    color += 222.4922359499; // 360 / 1.61803...
    
    for (int i = 0; i < wallsize; ++i) {
        img_drawbox(img, xo + xa + i, yo + ya + i, xo + (2 * xa) - i, yo + (2 * ya) - i);
        if (i >= xa) break;
    }
    // img_drawbox(img, xo + xa, yo + ya, xo + (2 * xa), yo + (2 * ya));
    
    
    double const xa2 = xa * 2;
    double const ya2 = ya * 2;
    
    sierpinski_carpet(img,       xo,       yo, xa, ya, depth - 1, wallsize - 1);
    sierpinski_carpet(img,  xo + xa,       yo, xa, ya, depth - 1, wallsize - 1);
    sierpinski_carpet(img, xo + xa2,       yo, xa, ya, depth - 1, wallsize - 1);
    sierpinski_carpet(img, xo + xa2,  yo + ya, xa, ya, depth - 1, wallsize - 1);
    sierpinski_carpet(img, xo + xa2, yo + ya2, xa, ya, depth - 1, wallsize - 1);
    sierpinski_carpet(img,  xo + xa, yo + ya2, xa, ya, depth - 1, wallsize - 1);
    sierpinski_carpet(img,       xo, yo + ya2, xa, ya, depth - 1, wallsize - 1);
    sierpinski_carpet(img,       xo,  yo + ya, xa, ya, depth - 1, wallsize - 1);

}

void writeimg(struct plot_img *img, struct plot_data const *const data,
              unsigned int center_x, unsigned int center_y,
              double scale_x, double scale_y)
{
    assert(center_x < img->width);
    assert(center_y < img->height);
    
    double x0, y0, x1, y1, d, t;
    unsigned int iter;
    
    switch (data->type) {
    case PLOT_TYPE_FX:
        x0 = data->fx.xmin;
        x1 = data->fx.xmin;
        y0 = data->fx.fn(x0);
        d = (1 / (data->fx.xmax - data->fx.xmin));
        
        do {
            x1 += d;
            y1 = data->fx.fn(x1);
            img_drawline(img, center_x + (x0 * scale_x),
                              center_y - (y0 * scale_y),
                              center_x + (x1 * scale_x),
                              center_y - (y1 * scale_y));
            y0 = y1;
            x0 = x1;
        } while (x1 < data->fx.xmax);
        break;
    
    case PLOT_TYPE_FY:
        y0 = data->fy.ymin;
        y1 = data->fy.ymin;
        x0 = data->fy.fn(y0);
        d = (1 / (data->fy.ymax - data->fy.ymin));
        
        do {
            y1 += d;
            x1 = data->fy.fn(y1);
            img_drawline(img, center_x + (x0 * scale_x),
                              center_y - (y0 * scale_y),
                              center_x + (x1 * scale_x),
                              center_y - (y1 * scale_y));
            y0 = y1;
            x0 = x1;
        } while (y1 < data->fy.ymax);
        break;
    
    case PLOT_TYPE_RECURSIVE:
        y0 = data->recursive.xinit;
        x0 = data->recursive.yinit;
        iter = data->recursive.iterations;
        
        while (iter--) {
            y1 = data->recursive.fy(y0, x0);
            x1 = data->recursive.fx(y0, x0);
            img_drawline(img, center_x + (x0 * scale_x),
                              center_y - (y0 * scale_y),
                              center_x + (x1 * scale_x),
                              center_y - (y1 * scale_y));
            y0 = y1;
            x0 = x1;
        }
        break;
    
    case PLOT_TYPE_PARAMETRIC:
        t = data->parametric.tmin;
        d = data->parametric.tinc;
        x0 = data->parametric.fx(t);
        y0 = data->parametric.fy(t);
        
        while ((t += d) < data->parametric.tmax) {
            x1 = data->parametric.fx(t);
            y1 = data->parametric.fy(t);
            img_drawline(img, center_x + (x0 * scale_x),
                              center_y - (y0 * scale_y),
                              center_x + (x1 * scale_x),
                              center_y - (y1 * scale_y));
            x0 = x1;
            y0 = y1;
        }
        break;
    
    default:
        assert(0);
    }
}

double id(double x) { return x; }

double g_mult = 1;

double sinmult(double x) {
    return cos(x * g_mult);
}

int main(int argc, char **argv)
{
    if (pthread_mutex_init(&g_tasks_mutex, NULL)) {
        printf("mutex init fail\n");
        exit(-1);
    }
    
    maketask(3, 4, 1, 0);
    maketask(5, 8, 1, 0);
    maketask(6, 10, 2, 0);
    maketask(9, 15, 4, 0);
    
    struct plot_img img;
    struct plot_data data;
    
    img_init(&img, 1500, 1500, 0, 0, 0, 255, 255, 255); //1280 * 800 = 1024000
    
    for (double c = 0; c < 360; c += 60) {
        data_init_parametric(&data, sin, sinmult, 0, 6.283, 0.0001);
        img_setpenhsv(&img, c, 1.0, 1.0);
        writeimg(&img, &data, 800, 800, 600, 600);
        g_mult += 1;
    }
    
   // data_init_parametric(&data, sin, singold, 0, 6.283, 0.0001);
   // img_setpenhsv(&img, 240, 1.0, 1.0);
   // writeimg(&img, &data, 800, 800, 600, 600);
    
    // sierpinski_carpet(&img, 0, 0, img.width, img.height, 6, 8);
//     draw_function(&img, 0, 16, demand, 10, 390, 16.0, 16.0);
//     
//     img_setpenrgb(&img, 255, 0, 0);
//     draw_function(&img, 0, 16, workload, 10, 390, 16.0, 16.0);
//     
//     img_setpenrgb(&img, 0, 0, 255);
//     draw_function(&img, 0, 16, id, 10, 390, 16.0, 16.0);
    
    img_writeout(&img, "testout.png", "Foo!");
 
    img_destroy(&img);
    
//     printf("success!\n");
}




