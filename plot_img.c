#include "plot.h"
#include <string.h>
#include <assert.h>
#include <math.h>

#ifndef NDEBUG
#include <time.h>
#endif

#define LPLOT_INCR_POOLALLOC 4096
#define LPLOT_IMG_ENTER_CRITICAL(_img) do { if (pthread_mutex_lock(&_img->mutex)) return PLOT_STATUS_PTHREAD_ERR; } while (0)
#define LPLOT_IMG_EXIT_CRITICAL(_img) do { if (pthread_mutex_unlock(&_img->mutex)) return PLOT_STATUS_PTHREAD_ERR; } while (0)

/* STATIC HELPER FUNCTIONS */

/* performs a radial wrap to the range. 
used for normalizing angles to 0-360 */
static double angle_wrap(double a, double range)
{
    double adj = range;
    
    if ((a < range) && (a >= 0.0)) return a;
    else if (a < 0.0) adj = -adj;
    
    return angle_wrap(a - adj, range);
}

static inline double range(double a, double min, double max)
{
    if (a < min)      return min;
    else if (a > max) return max;
    else              return a;
}

/* fill a row with the appropriate background color */
static void initrow(struct plot_img *img, unsigned int rownum)
{
    png_bytep buff = img->imgbuffer[rownum];
    
    for (int i = 0; i < img->width * 3; i += 3) {
        buff[i] = img->bg_color.r;
        buff[i + 1] = img->bg_color.g;
        buff[i + 2] = img->bg_color.b;
    }
}

/* gives the caller a pointer to a row buffer. Creates row buffers if it
   needs to. */
static inline png_bytep getrow(struct plot_img *img, unsigned int y)
{
    if (img->imgbuffer[y] != NULL)
        return img->imgbuffer[y];
    
    else if (img->pool.blocks_cached > 0) {
        img->pool.blocks_cached -= 1;
        img->imgbuffer[y] = img->pool.ptr;
        img->pool.ptr += (sizeof(png_byte) * img->width * 3);
        initrow(img, y);
        return img->imgbuffer[y];
        
    } else {
        unsigned int const onerowsz = sizeof(png_byte) * img->width * 3;
        unsigned int allocsz = onerowsz;
        unsigned int blocksleft = (img->height - img->pool.total_rows);
        assert (blocksleft > 0);
        

        // rows to alloc = msb of however many rows are left to alloc
        unsigned long long k;
        unsigned int blhalf = blocksleft >> 1;
        for (k = 0x100000000; k > 1; k = k >> 1) {
            if (k & blhalf) break;
        }
        
        allocsz = LPLOT_INCR_POOLALLOC;
        while (allocsz < (onerowsz * k)) allocsz += LPLOT_INCR_POOLALLOC;
        
        img->pool.blocks_cached = allocsz / onerowsz * 1;
        allocsz = img->pool.blocks_cached * onerowsz;
        
#ifndef NDEBUG
        img->pool.total_bytes += allocsz;
#endif
        
        img->pool.ptr = malloc(allocsz);
        img->pool.total_rows += img->pool.blocks_cached;
        img->pool.blocks_cached -= 1;
        img->imgbuffer[y] = img->pool.ptr;
        img->freerows[y] = 1;
#ifndef NDEBUG
        img->pool.total_allocs += 1;
#endif
        img->pool.ptr += onerowsz;
        
        initrow(img, y); //TODO null checks for allocation
        
        return img->imgbuffer[y];
    }
}

static inline void convert_hsv(double h, double s, double v, unsigned char *r, unsigned char *g, unsigned char *b)
{   // https://en.wikipedia.org/wiki/HSL_and_HSV#Conversion_RGB_to_HSV_used_commonly_in_software_programming
    h = angle_wrap(h, 360.0);
    s = range(s, 0.0, 1.0) * 255.0;
    v = range(v, 0.0, 1.0);
    
    double hp = h / 60.0;   // hp is H' in wikipedia's notation
    double c = v * s;
    double xx = c * (1.0 - fabs(fmod(hp, 2) - 1));
    
    switch ((int)hp) {
    case 0:
        *r = c;
        *g = xx;
        *b = 0;
        break;
    
    case 1:
        *r = xx;
        *g = c;
        *b = 0;
        break;
    
    case 2:
        *r = 0;
        *g = c;
        *b = xx;
        break;
    
    case 3:
        *r = 0;
        *g = xx;
        *b = c;
        break;
    
    case 4:
        *r = xx;
        *g = 0;
        *b = c;
        break;
    
    case 5:
        *r = c;
        *g = 0;
        *b = xx;
        break;
    
    default:
        plot_debugmsg("lplot error: unexpected position in case structure!\n");
        assert(0);
    }
}

static inline void set_point(struct plot_img *img, unsigned int x, unsigned int y)
{
    unsigned int xx = x * 3;
    png_bytep row = getrow(img, y);
    row[xx] = img->pen_color.r;
    row[xx + 1] = img->pen_color.g;
    row[xx + 2] = img->pen_color.b;
}

/* draws paths, moving only at 45 and 90 degree angles. helpful for correcting
 small errors in the endpoint of line_draw */
static void line_draw_hlpr(struct plot_img *img, unsigned int xa, unsigned int ya,
                                                 unsigned int xb, unsigned int yb)
{
    set_point(img, xa, ya);
    
    if ((xa == xb) && (ya == yb)) return;
    
    int dx = abs(xb - xa);
    int dy = abs(yb - ya);
    int adj_x = xa > xb ? -1 : 1;
    int adj_y = ya > yb ? -1 : 1;
    
    if (dx > dy)
        line_draw_hlpr(img, xa + adj_x, ya, xb, yb);
    else if (dy > dx)
        line_draw_hlpr(img, xa, ya + adj_y, xb, yb);
    else
        line_draw_hlpr(img, xa + adj_x, ya + adj_y, xb, yb);
}



static void line_draw(struct plot_img *img, unsigned int xa, unsigned int ya,
                                            unsigned int xb, unsigned int yb)
{
    if ((xa == xb) || (ya == yb)) {
        line_draw_hlpr(img, xa, ya, xb, yb);
        
    } else {
        int dy = yb - ya;
        int dx = xb - xa;
        
        double step;
        if (abs(dx) >= abs(dy)) step = abs(dx);
        else step = abs(dy);
        
        double sdx = dx / step;
        double sdy = dy / step;
        double x = xa, y = ya;
        for (unsigned int i = 1; i <= step; ++i) {
            set_point(img, x, y);
            y += sdy;
            x += sdx;
        }
        
        // cover for any small error in the path end
        line_draw_hlpr(img, x, y, xb, yb);
    }
}


/* EXTERNALLY-VISIBLE FUNCTIONS */

enum plot_status img_init(struct plot_img *img, unsigned int width, unsigned int height,
                          unsigned char bg_r, unsigned char bg_g, unsigned char bg_b,
                          unsigned char pen_r, unsigned char pen_g, unsigned char pen_b)
{
    if (img == NULL) return PLOT_STATUS_NULL_PTR;
    
    pthread_mutexattr_t attr;
    if (pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK)) return PLOT_STATUS_PTHREAD_ERR;
    if (pthread_mutex_init(&img->mutex, &attr)) return PLOT_STATUS_PTHREAD_ERR;
    
    LPLOT_IMG_ENTER_CRITICAL(img);
    
//     img->imgbuffer = calloc(1, sizeof(png_bytep) * height);
    img->imgbuffer = malloc(sizeof(png_bytep) * height);
    memset(img->imgbuffer, 0, sizeof(png_bytep) * height);
    
    img->freerows = malloc(sizeof(unsigned char) * height);
    memset(img->freerows, 0, sizeof(unsigned char) * height);
    
    img->width = width;
    img->height = height;
    
    img->bg_color.r = bg_r;
    img->bg_color.g = bg_g;
    img->bg_color.b = bg_b;
    
    img->pen_color.r = pen_r;
    img->pen_color.g = pen_g;
    img->pen_color.b = pen_b;
    
    img->pool.blocks_cached = 0;
    img->pool.total_rows = 0;
#ifndef NDEBUG
    img->pool.total_bytes = 0;
    img->pool.total_allocs = 0;
#endif
    
    LPLOT_IMG_EXIT_CRITICAL(img);
    
    return PLOT_STATUS_OK;
}

enum plot_status img_quickinit(struct plot_img *img, unsigned int width, unsigned int height)
{
    return img_init(img, width, height, 255, 255, 255, 0, 0, 0);
}

enum plot_status img_setpenrgb(struct plot_img *img,
                               unsigned char r, unsigned char g, unsigned char b)
{
    if (img == NULL) return PLOT_STATUS_NULL_PTR;
    
    LPLOT_IMG_ENTER_CRITICAL(img);
    
    img->pen_color.r = r;
    img->pen_color.g = g;
    img->pen_color.b = b;
    
    LPLOT_IMG_EXIT_CRITICAL(img);
    
    return PLOT_STATUS_OK;
}

enum plot_status img_setpenhsv(struct plot_img *img,
                               double h, double s, double v)
{
    if (img == NULL) return PLOT_STATUS_NULL_PTR;
    
    unsigned char r, g, b;

    convert_hsv(h, s, v, &r, &g, &b);
    
    return img_setpenrgb(img, r, g, b);
}


enum plot_status img_drawpoint(struct plot_img *img, unsigned int x, unsigned int y)
{
    if (img == NULL) return PLOT_STATUS_NULL_PTR;
    if ((x >= img->width) || (y >= img->height)) return PLOT_STATUS_INVALID_COORD;
    
    LPLOT_IMG_ENTER_CRITICAL(img);
    
    set_point(img, x, y);
    
    LPLOT_IMG_EXIT_CRITICAL(img);
    
    return PLOT_STATUS_OK;
}

enum plot_status img_drawline(struct plot_img *img, unsigned int xa, unsigned int ya,
                                                    unsigned int xb, unsigned int yb)
{
    if (img == NULL) return PLOT_STATUS_NULL_PTR;
    if ((xa >= img->width) || (xb >= img->width) ||
        (ya >= img->height) || (yb >= img->height)) return PLOT_STATUS_INVALID_COORD;
    
    LPLOT_IMG_ENTER_CRITICAL(img);
    
    line_draw(img, xa, ya, xb, yb);
    
    LPLOT_IMG_EXIT_CRITICAL(img);
    
    return PLOT_STATUS_OK;
}

enum plot_status img_drawbox(struct plot_img *img, unsigned int xa, unsigned int ya,
                                                   unsigned int xb, unsigned int yb)
{
    if(img == NULL) return PLOT_STATUS_NULL_PTR;
    if ((xa >= img->width) || (xb >= img->width) ||
        (ya >= img->height) || (yb >= img->height)) return PLOT_STATUS_INVALID_COORD;
    
    LPLOT_IMG_ENTER_CRITICAL(img);
    
    line_draw(img, xa, ya, xa, yb);
    line_draw(img, xb, ya, xb, yb);
    line_draw(img, xa, ya, xb, ya);
    line_draw(img, xa, yb, xb, yb);
    
    LPLOT_IMG_EXIT_CRITICAL(img);
    
    return PLOT_STATUS_OK;
}

enum plot_status img_writeout(struct plot_img *img, char* filename, char *title)
{
    if (img == NULL) return PLOT_STATUS_NULL_PTR;

    LPLOT_IMG_ENTER_CRITICAL(img);
    
#ifndef NDEBUG
    long int ms0, ms1 = 0;
    struct timespec spec0, spec1;
    clock_gettime(CLOCK_REALTIME, &spec0);
    ms0 = round(spec0.tv_nsec / 1.0e6);
#endif
    
    int code = PLOT_STATUS_OK, mutexreleased = 0;
    FILE *fp = NULL;
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    unsigned int rowlen = img->width * 3;
    png_byte emptyrow[rowlen];
    
    for (int i = 0; i < rowlen; i += 3) {
        emptyrow[i] = img->bg_color.r;
        emptyrow[i + 1] = img->bg_color.g;
        emptyrow[i + 2] = img->bg_color.b;
    }
    
    /* Open file for writing (binary mode) */
    fp = fopen(filename, "wb");
    if (fp == NULL) {
        code = PLOT_STATUS_FILE_OPEN_ERR;
        goto finalise;
    }

    /* Initialize write structure */
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        code = PLOT_STATUS_ALLOC_ERR;
        goto finalise;
    }

    /* Initialize info structure */
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        code = PLOT_STATUS_ALLOC_ERR;
        goto finalise;
    }

    /* Setup Exception handling */
    if (setjmp(png_jmpbuf(png_ptr))) {
        code = PLOT_STATUS_ERR;
        goto finalise;
    }

    png_init_io(png_ptr, fp);

    /* Write header (8 bit colour depth) */
    png_set_IHDR(png_ptr, info_ptr, img->width, img->height,
            8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
            PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    /* Set title */ 
    if (title != NULL) {
        png_text title_text;
        title_text.compression = PNG_TEXT_COMPRESSION_NONE;
        title_text.key = "Title";
        title_text.text = title;
        png_set_text(png_ptr, info_ptr, &title_text, 1);
    }

    png_write_info(png_ptr, info_ptr);
    
    for (png_bytep *row = img->imgbuffer; row < img->imgbuffer + img->height; ++row) {
        if ((*row) != NULL) {
            png_write_row(png_ptr, *row);
            
        } else {
            png_write_row(png_ptr, emptyrow);
        }
        
    }

    png_write_end(png_ptr, NULL);
    
//     LPLOT_IMG_EXIT_CRITICAL(img);
    if (pthread_mutex_unlock(&img->mutex)) goto finalise;
    else {
        mutexreleased = 1;
#ifndef NDEBUG
        clock_gettime(CLOCK_REALTIME, &spec1);
        ms1 = round(spec1.tv_nsec / 1.0e6);
#endif
    }
    
    finalise:
    if (fp != NULL) fclose(fp);
    if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
    if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, &info_ptr);
    
    if (mutexreleased == 0) LPLOT_IMG_EXIT_CRITICAL(img);
#ifndef NDEBUG
    if (ms1 == 0) ms1 = round(spec1.tv_nsec / 1.0e6);
#endif
    plot_debugmsg("img_writeout held mutex for %ld ms\n", (ms1 - ms0));

    return code;
}

enum plot_status img_destroy(struct plot_img *img)
{
    if (img == NULL) return PLOT_STATUS_NULL_PTR;
    
    LPLOT_IMG_ENTER_CRITICAL(img);
    
    if (img->imgbuffer != NULL) {
        int dofreerows = (img->freerows != NULL);
        
        for (int i = 0; i < img->height; ++i) {
            png_bytep row = img->imgbuffer[i];
            if ((row != NULL) && (dofreerows && (img->freerows[i]))) {
                plot_debugmsg("freeing row #%d\n", i);
                free(row);
                img->freerows[i] = 0;
            }
        }
        
        free(img->imgbuffer);
        
        if (dofreerows) free(img->freerows);
    }
    
    img->imgbuffer = NULL;
    img->freerows = NULL;
    
    plot_debugmsg("Total bytes allocated for rows: %d\n", img->pool.total_bytes);
    plot_debugmsg("Total calls to malloc() for rows: %d\n", img->pool.total_allocs);
    
    LPLOT_IMG_EXIT_CRITICAL(img);
    
    pthread_mutex_destroy(&img->mutex);
    
    return PLOT_STATUS_OK;
}
