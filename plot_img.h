#ifndef _PLOT_IMG_H_
#define _PLOT_IMG_H_

/**
 * @file plot_img.h
 * @brief API for direct manipulation of Tinyplot images
 * 
 * Although Tinyplot is meant, obviously, for plotting graphs and the like, its image
 * manipulation backend can also be used directly for drawing simple images. This interface
 * allows the user to directly access the Tinyplot library's image drawing facilities.
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief initialize a plot_img with some sane defaults for background color and pen color.
 * 
 * @param img a pointer to a plot_img struct.
 * @param width the width of the output image.
 * @param height the width of the output image.
 */
enum plot_status img_quickinit(struct plot_img *img, unsigned int width, unsigned int height);

/**
 * @brief initialize a plot_img, explicitly specifying pen and background colors.
 * 
 * @param img a pointer to a plot_img struct.
 * @param width the width of the output image.
 * @param height the width of the output image.
 * @param bg_r red channel of background color (0-255)
 * @param bg_g green channel of background color (0-255)
 * @param bg_b blue channel of background color (0-255)
 * @param pen_r red channel of pen color (0-255)
 * @param pen_g green channel of pen color (0-255)
 * @param pen_b blue channel of pen color (0-255)
 */
enum plot_status img_init(struct plot_img *img, unsigned int width, unsigned int height,
                          unsigned char bg_r, unsigned char bg_g, unsigned char bg_b,
                          unsigned char pen_r, unsigned char pen_g, unsigned char pen_b);

/**
 * @brief change the pen color for the plot_img using RGB values.
 * 
 * @param img a pointer to a plot_img struct.
 * @param r red channel of pen color (0-255)
 * @param g green channel of pen color (0-255)
 * @param b blue channel of pen color (0-255)
 */
enum plot_status img_setpenrgb(struct plot_img *img,
                               unsigned char r, unsigned char g, unsigned char b);

/**
 * @brief change the pen color for the plot_img using HSV values.
 * 
 * @param img a pointer to a plot_img struct.
 * @param h hue, expressed as a position on a 360 degree color wheel. (0-360 is a complete spectrum)
 * @param s saturation, expressed proportionally (0.0-1.0)
 * @param v value, expressed proportionally (0.0-1.0)
 */
enum plot_status img_setpenhsv(struct plot_img *img,
                               double h, double s, double v);

/**
 * @brief set a single pixel in the plot_img to the current pen color.
 * 
 * @param img a pointer to a plot_img struct.
 * @param x the horizontal coordinate of the pixel to draw. (left = 0)
 * @param y the vertical coordinate of the pixel to draw. (top = 0)
 */
enum plot_status img_drawpoint(struct plot_img *img, unsigned int x, unsigned int y);

/**
 * @brief draw a line in the plot_img using the current pen color.
 * 
 * draws a straight line from (xa, ya) to (xb, yb).
 * 
 * @param img a pointer to a plot_img struct.
 * @param x the horizontal coordinate of the pixel to draw. (left = 0)
 * @param y the vertical coordinate of the pixel to draw. (top = 0)
 */
enum plot_status img_drawline(struct plot_img *img, unsigned int xa, unsigned int ya,
                                                    unsigned int xb, unsigned int yb);

enum plot_status img_drawbox(struct plot_img *img, unsigned int xa, unsigned int ya,
                                                   unsigned int xb, unsigned int yb);

enum plot_status img_writeout(struct plot_img *img, char* filename, char *title);

enum plot_status img_destroy(struct plot_img *img);

#ifdef __cplusplus
}
#endif

#endif
