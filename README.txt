Tinyplot is a simple and small C library for plotting data. It uses libpng to
output its images, and it uses pthread mutexes to ensure that multithreaded
programs can use Tinyplot safely.

