#ifndef _PLOT_DATA_H_
#define _PLOT_DATA_H_

#include "plot.h"

/**
 * @file plot_data.h
 * @brief create functions to be plotted.
 */

#ifdef __cplusplus
extern "C" {
#endif


enum plot_status data_init_fx(struct plot_data *data, double (*fn)(double),
                              double xmin, double xmax);

enum plot_status data_init_fy(struct plot_data *data, double (*fn)(double),
                              double ymin, double ymax);

enum plot_status data_init_recursive(struct plot_data *data,
                                     double (*fx)(double,double), double (*fy)(double,double),
                                     double xinit, double yinit, unsigned int iterations);

enum plot_status data_init_parametric(struct plot_data *data, double (*fx)(double), double (*fy)(double),
                                      double tmin, double tmax, double tinc);


#ifdef __cplusplus
}
#endif

#endif
