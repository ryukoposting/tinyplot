#ifndef _PLOT_H_
#define _PLOT_H_

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <png.h>

#ifdef __cplusplus
extern "C" {
#endif


enum plot_status {
    PLOT_STATUS_OK = 0,
    PLOT_STATUS_ERR,
    PLOT_STATUS_INVALID_COORD,
    PLOT_STATUS_PTHREAD_ERR,
    PLOT_STATUS_FILE_OPEN_ERR,
    PLOT_STATUS_ALLOC_ERR,
    PLOT_STATUS_NULL_PTR
};

enum plot_type {
    PLOT_TYPE_FX,
    PLOT_TYPE_FY,
    PLOT_TYPE_RECURSIVE,
    PLOT_TYPE_PARAMETRIC,
    PLOT_TYPE_POLAR
};

struct plot_data {
    enum plot_type type;
    union {
        struct {
            double (*fn)(double);
            double xmin, xmax;
        } fx;
        
        struct {
            double (*fn)(double);
            double ymin, ymax;
        } fy;
        
        struct {
            double (*fx)(double, double);
            double (*fy)(double, double);
            double xinit, yinit;
            unsigned int iterations;
        } recursive;
        
        struct {
            double (*fx)(double);
            double (*fy)(double);
            double tmin, tmax, tinc;
        } parametric;
    };
};

struct plot_img {
    unsigned int width;
    unsigned int height;
    pthread_mutex_t mutex;
    
    struct {
        png_bytep ptr;
        unsigned int blocks_cached;
        unsigned int total_rows;
#ifndef NDEBUG
        unsigned int total_allocs;
        unsigned int total_bytes;
#endif
    } pool;
    
    struct {
        unsigned char r, g, b;
    } bg_color;
    
    struct {
        unsigned char r, g, b;
    } pen_color;
    
    png_bytep *imgbuffer;
    unsigned char *freerows;
};


#ifdef NDEBUG
#define plot_debugmsg(...) ((void)0)
#else
#define plot_debugmsg(...) printf(__VA_ARGS__)
#endif

static inline enum plot_type plot_data_gettype(struct plot_data const *const plot) { return plot->type; }

static inline unsigned char plot_img_getbg_r(struct plot_img const *const img) { return img->bg_color.r; }
static inline unsigned char plot_img_getbg_g(struct plot_img const *const img) { return img->bg_color.g; }
static inline unsigned char plot_img_getbg_b(struct plot_img const *const img) { return img->bg_color.b; }
static inline unsigned char plot_img_getpen_r(struct plot_img const *const img) { return img->pen_color.r; }
static inline unsigned char plot_img_getpen_g(struct plot_img const *const img) { return img->pen_color.g; }
static inline unsigned char plot_img_getpen_b(struct plot_img const *const img) { return img->pen_color.b; }
static inline unsigned int plot_img_getwidth(struct plot_img const *const img) { return img->width; }
static inline unsigned int plot_img_getheight(struct plot_img const *const img) { return img->height; }


#ifdef __cplusplus
}
#endif

#include "plot_img.h"
#include "plot_data.h"

#endif
