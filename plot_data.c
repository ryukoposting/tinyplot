#include "plot.h"

enum plot_status data_init_fx(struct plot_data *data, double (*fn)(double),
                              double xmin, double xmax)
{
    if ((data == NULL) || (fn == NULL)) return PLOT_STATUS_NULL_PTR;
    if (xmin > xmax) return data_init_fx(data, fn, xmax, xmin);
    
    data->type = PLOT_TYPE_FX;
    
    data->fx.fn = fn;
    data->fx.xmin = xmin;
    data->fx.xmax = xmax;
    
    return PLOT_STATUS_OK;
}


enum plot_status data_init_fy(struct plot_data *data, double (*fn)(double),
                              double ymin, double ymax)
{
    if ((data == NULL) || (fn == NULL)) return PLOT_STATUS_NULL_PTR;
    if (ymin > ymax) return data_init_fx(data, fn, ymax, ymin);
    
    data->type = PLOT_TYPE_FY;
    
    data->fy.fn = fn;
    data->fy.ymin = ymin;
    data->fy.ymax = ymax;
    
    return PLOT_STATUS_OK;
}


enum plot_status data_init_recursive(struct plot_data *data,
                                     double (*fx)(double,double), double (*fy)(double,double),
                                     double xinit, double yinit, unsigned int iterations)

{
    if ((data == NULL) || (fx == NULL) || (fy == NULL)) return PLOT_STATUS_NULL_PTR;
    
    data->type = PLOT_TYPE_RECURSIVE;
    
    data->recursive.fx = fx;
    data->recursive.fy = fy;
    data->recursive.xinit = xinit;
    data->recursive.yinit = yinit;
    data->recursive.iterations = iterations;
    
    return PLOT_STATUS_OK;
}


enum plot_status data_init_parametric(struct plot_data *data, double (*fx)(double), double (*fy)(double),
                                      double tmin, double tmax, double tinc)
{
    if ((data == NULL) || (fx == NULL) || (fy == NULL)) return PLOT_STATUS_NULL_PTR;
    if (tmin > tmax) return data_init_parametric(data, fx, fy, tmax, tmin, tinc);
    
    data->type = PLOT_TYPE_PARAMETRIC;
    
    data->parametric.fx = fx;
    data->parametric.fy = fy;
    data->parametric.tmin = tmin;
    data->parametric.tmax = tmax;
    data->parametric.tinc = tinc;
    
    return PLOT_STATUS_OK;
}
